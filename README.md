### Server setup ###
* apt-get update
* apt-get upgrade
* apt-get install curl
* curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
* apt-get install nodejs
* apt-get install mariadb-server
* apt-get install mongodb-org
* apt-get install apache2
* npm install -g pm2

### API Endpoints ###

* /users ⇒ retourne une liste de users (filtrée)
GET
POST : paramètre ⇒ user à insérer
* /users/{id} ⇒ retourne un user avec son id
GET
DELETE
PUT : paramètre ⇒ user avec ses nouvelles valeurs
* /users/{id}/evaluations ⇒ retourne les évaluations qu’un user a reçues
GET
POST : paramètre ⇒ évaluation à insérer
* /users/{id}/evaluations/{id} ⇒ retourne l’évaluation qu’un user a reçue d’un autre user
GET
DELETE
PUT : paramètre ⇒ évaluation avec ses nouvelles valeurs
* /users/{id}/events ⇒ retourne la liste des events passés et à venir d’un user
GET
POST : paramètre ⇒ participation à un event à insérer
* /users/{id}/events/{id} ⇒ retourne un  event passé ou à venir d’un user
GET
DELETE
PUT : paramètre : participation à un event avec ses nouvelles valeurs
* /users/{id}/groups ⇒ retourne la liste des groupes rejoints par un user
GET
POST : paramètre : inscription à un nouveau groupe
* /users/{id}/groups/{id} ⇒ retourne un groupe dont l’user est membre

* /users/{id}/medias ⇒ retourne la liste des fichiers qu’un user possède (photos, vidéos..)
* /users/{id}/medias/{id} ⇒ retourne un fichier précis d’un user
* /users/{id}/friends ⇒ retourne la liste des contacts d’un user
* /users/{id}/friends/{id} ⇒ retourne un contact d’un user
* /users/{id}/notifications ⇒ retourne la liste des notifications d’un user
* /users/{id}/notifications/{id} ⇒ retourne une notification d’un user
* /events ⇒ retourne une liste d’events (filtrée)
* /events/{id} ⇒ retourne un event avec son id
* /events/{id}/evaluations ⇒ retourne les évaluations qu’un event a reçues
* /events/{id}/evaluations/{id} ⇒ retourne l’évaluation qu’un event a reçue d’un user
* /events/{id}/medias ⇒ retourne la liste des fichiers qu’un event possède (photos, vidéos..)
* /events/{id}/medias/{id} ⇒ retourne un fichier précis d’un event
* /events/{id}/participants ⇒ retourne la liste des participants d’un event
* /events/{id}/participants/{id} ⇒ retourne un participant d’un event
* /groups ⇒ retourne une liste de groupes d’users (filtrée)
* /groups/{id} ⇒ retourne un groupe avec son id
* /groups/{id}/medias ⇒ retourne la liste des fichiers qu’un groupe possède (photos, vidéos..)
* /groups/{id}/medias/{id} ⇒ retourne un fichier précis d’un groupe
* /groups/{id}/members ⇒ retourne la liste des membres d’un groupe
* /groups/{id}/members/{id} ⇒ retourne un membre d’un groupe