const router = require('express').Router();

module.exports = (app) => {
    // FINDALL - GET ALL GROUPS
    router.get('/',
        app.actions.groups.findAll
    );

    // FINDONE - GET ONE GROUP
    router.get('/:groupId',
        app.middlewares.parsers.groups,
        app.actions.groups.findOne
    );

    // CREATE - NEW GROUP
    router.post('/',
        app.middlewares.security.isAuthenticated,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.groups,
        app.actions.groups.create
    );

    // UPDATE - EDIT ONE GROUP
    router.put('/:groupId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.groups,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.groups,
        app.actions.groups.update
    );

    // REMOVE - DELETE ONE GROUP
    router.delete('/:groupId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.groups,
        app.actions.groups.remove
    );

    // FINDALL - GET ALL GROUP MEMBERS
    router.get('/:groupId/members',
        app.middlewares.parsers.groups,
        app.actions.groups.members.findAll
    );

    // CREATE - NEW GROUP MEMBER
    router.post('/:groupId/members',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.groups,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.status,
        app.actions.groups.members.create
    );

    // REMOVE - DELETE GROUP MEMBER
    router.delete('/:groupId/members',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.groups,
        app.actions.groups.members.remove
    );

    // UPDATE - EDIT GROUP MEMBER
    router.put('/:groupId/members/:memberId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.groups,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.status,
        app.actions.groups.members.update
    );

    // FINDALL - GET ALL GROUP MEDIAS
    router.get('/:groupId/medias',
        app.middlewares.parsers.groups,
        app.actions.groups.medias.findAll
    );

    // CREATE - NEW GROUP MEDIA
    router.post('/:groupId/medias',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.groups,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.medias,
        app.actions.groups.medias.create
    );

    // REMOVE - DELETE GROUP MEDIA
    router.delete('/:groupId/medias/:mediaId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.groups,
        app.middlewares.parsers.medias,
        app.actions.groups.medias.remove
    );

    // UPDATE - EDIT GROUP MEDIA
    router.put('/:groupId/medias/:mediaId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.groups,
        app.middlewares.parsers.medias,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.medias,
        app.actions.groups.medias.update
    );

    // CREATE - ADD ONE GROUP TAG
    router.post('/:groupId/tags/:tagId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.groups,
        app.actions.groups.tags.create
    );

    // REMOVE - DELETE ONE GROUP TAG
    router.delete('/:groupId/tags/:tagId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.groups,
        app.actions.groups.tags.remove
    );
    
    return router;
};
