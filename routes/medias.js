const router = require('express').Router();

module.exports = (app) => {
    // FINDALL - GET ALL MEDIAS
    router.get('/',
        app.middlewares.security.isAuthenticated,
        app.actions.medias.findAll
    );

    // FINDONE - GET ONE GROUP
    router.get('/:mediaId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.medias,
        app.actions.medias.findOne
    );

    // CREATE NEW MEDIA
    router.post('/',
        app.middlewares.security.isAuthenticated,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.medias,
        app.actions.medias.create
    );

    // MEDIAS UPLOADING
    router.post('/upload',
        app.middlewares.security.isAuthenticated,
        app.middlewares.uploader.single("picture"),
        app.actions.medias.create
    );

    return router;
};
