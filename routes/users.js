const router = require('express').Router();

module.exports = (app) => {
    // FINDALL - GET ALL USERS
    router.get('/',
        app.middlewares.security.isAuthenticated,
        app.actions.users.findAll
    );

    // FINDONE - GET ONE USER
    router.get('/:userId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.actions.users.findOne
    );

    // CREATE - NEW USER
    router.post('/',
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.users,
        app.actions.users.create
    );

    // REMOVE - DELETE ONE USER
    router.delete('/:userId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.actions.users.remove
    );

    // FINDALL - GET ALL EVENTS OWNED BY USER
    router.get('/:userId/events',
        app.middlewares.parsers.users,
        app.actions.users.events.findAll
    );

    // FINDALL - GET ALL EVALUATIONS
    router.get('/:userId/evaluations',
        app.middlewares.parsers.users,
        app.actions.users.evaluations.findAll
    );

    // FINDONE - GET ONE EVALUATION
    router.get('/:userId/evaluations/:evaluationId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.middlewares.parsers.evaluations,
        app.actions.users.evaluations.findOne
    );

    // CREATE - NEW USER EVALUATION
    router.post('/:userId/evaluations',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.evaluations,
        app.actions.users.evaluations.create
    );

    // REMOVE - DELETE USER EVALUATION
    router.delete('/:userId/evaluations/:evaluationId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.middlewares.parsers.evaluations,
        app.actions.users.evaluations.remove
    );

    // UPDATE - EDIT USER EVALUATION
    router.put('/:userId/evaluations/:evaluationId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.middlewares.parsers.evaluations,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.evaluations,
        app.actions.users.evaluations.update
    );

    // FINDALL - GET ALL PARTICIPATIONS
    router.get('/:userId/participations',
        app.middlewares.parsers.users,
        app.actions.users.participations.findAll
    );

    // FINDALL - GET ALL CONTACTS
    router.get('/:userId/contacts',
        app.middlewares.parsers.users,
        app.actions.users.contacts.findAll
    );

    // CREATE - NEW USER CONTACT
    router.post('/:userId/contacts',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.actions.users.contacts.create
    );

    // REMOVE - DELETE USER CONTACT
    router.delete('/:userId/contacts/:contactId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.middlewares.parsers.contacts,
        app.actions.users.contacts.remove
    );

    // UPDATE - EDIT USER CONTACT
    router.put('/:userId/contacts/:contactId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.middlewares.parsers.contacts,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.status,
        app.actions.users.contacts.update
    );

    // FINDALL - GET ALL USER MEMBERSHIPS
    router.get('/:userId/memberships',
        app.middlewares.parsers.users,
        app.actions.users.memberships.findAll
    );

    // FINDALL - GET ALL GROUPS OWNED BY USER
    router.get('/:userId/groups',
        app.middlewares.parsers.users,
        app.actions.users.groups.findAll
    );

    // FINDALL - GET ALL USER MEDIAS
    router.get('/:userId/medias',
        app.middlewares.parsers.users,
        app.actions.users.medias.findAll
    );


    // CREATE - NEW USER MEDIA
    router.post('/:userId/medias',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.medias,
        app.actions.users.medias.create
    );

    // REMOVE - DELETE USER MEDIA
    router.delete('/:userId/medias/:mediaId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.middlewares.parsers.medias,
        app.actions.users.medias.remove
    );

    // UPDATE - EDIT USER MEDIA
    router.put('/:userId/medias/:mediaId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.middlewares.parsers.medias,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.medias,
        app.actions.users.medias.update
    );

    // FINDALL - GET ALL USER TAGS
    router.get('/:userId/tags',
        app.middlewares.parsers.events,
        app.actions.users.tags.findAll
    );

    // CREATE - ADD ONE USER TAG
    router.post('/:userId/tags/:tagId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.actions.users.tags.create
    );

    // REMOVE - DELETE ONE USER TAG
    router.delete('/:userId/tags/:tagId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.users,
        app.actions.users.tags.remove
    );
    
    return router;
};
