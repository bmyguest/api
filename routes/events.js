const router = require('express').Router();

module.exports = (app) => {
    // FINDALL - GET ALL EVENTS
    router.get('/',
        app.actions.events.findAll
    );

    // FINDONE - GET ONE EVENT
    router.get('/:eventId',
        app.middlewares.parsers.events,
        app.actions.events.findOne
    );

    // CREATE - NEW EVENT
    router.post('/',
        app.middlewares.security.isAuthenticated,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.events,
        app.actions.events.create
    );

    // UPDATE - EDIT ONE EVENT
    router.put('/:eventId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.events,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.events,
        app.actions.events.update
    );

    // REMOVE - DELETE ONE EVENT
    router.delete('/:eventId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.events,
        app.actions.events.remove
    );

    // FINDALL - GET ALL EVENT EVALUATIONS
    router.get('/:eventId/evaluations',
        app.middlewares.parsers.events,
        app.actions.events.evaluations.findAll
    );

    // CREATE - NEW EVENT EVALUATION
    router.post('/:eventId/evaluations',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.events,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.evaluations,
        app.actions.events.evaluations.create
    );

    // REMOVE - DELETE EVENT EVALUATION
    router.delete('/:eventId/evaluations/:evaluationId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.events,
        app.middlewares.parsers.evaluations,
        app.actions.events.evaluations.remove
    );

    // UPDATE - EDIT EVENT EVALUATION
    router.put('/:eventId/evaluations/:evaluationId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.events,
        app.middlewares.parsers.evaluations,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.evaluations,
        app.actions.events.evaluations.update
    );

    // FINDALL - GET ALL EVENT PARTICIPANTS
    router.get('/:eventId/participants',
        app.middlewares.parsers.events,
        app.actions.events.participants.findAll
    );

    // CREATE - NEW EVENT PARTICIPANT
    router.post('/:eventId/participants',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.events,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.status,
        app.actions.events.participants.create
    );

    // REMOVE - DELETE EVENT PARTICIPANT
    router.delete('/:eventId/participants/:participantId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.events,
        app.actions.events.participants.remove
    );

    // UPDATE - EDIT EVENT PARTICIPANT
    router.put('/:eventId/participants/:participantId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.events,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.status,
        app.actions.events.participants.update
    );

    // FINDALL - GET ALL EVENT MEDIAS
    router.get('/:eventId/medias',
        app.middlewares.parsers.events,
        app.actions.events.medias.findAll
    );

    // CREATE - NEW EVENT MEDIA
    router.post('/:eventId/medias',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.events,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.medias,
        app.actions.events.medias.create
    );

    // REMOVE - DELETE EVENT MEDIA
    router.delete('/:eventId/medias/:mediaId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.events,
        app.middlewares.parsers.medias,
        app.actions.events.medias.remove
    );

    // UPDATE - EDIT EVENT MEDIA
    router.put('/:eventId/medias/:mediaId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.events,
        app.middlewares.parsers.medias,
        app.middlewares.bodyparser.json(),
        app.middlewares.validation.medias,
        app.actions.events.medias.update
    );

    // FINDALL - GET ALL EVENT TAGS
    router.get('/:eventId/tags',
        app.middlewares.parsers.events,
        app.actions.events.tags.findAll
    );

    // CREATE - ADD ONE EVENT TAG
    router.post('/:eventId/tags/:tagId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.events,
        app.actions.events.tags.create
    );

    // REMOVE - DELETE ONE EVENT TAG
    router.delete('/:eventId/tags/:tagId',
        app.middlewares.security.isAuthenticated,
        app.middlewares.parsers.events,
        app.actions.events.tags.remove
    );

    return router;
};
