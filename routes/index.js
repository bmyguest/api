const cors = require('cors');
const express = require('express');

module.exports = (app) => {
    console.log('Loading routes...\n');

    app.use(cors());
    app.use('/events', require('./events')(app));
    app.use('/groups', require('./groups')(app));
    app.use('/medias', require('./medias')(app));
    app.use('/uploads', express.static('uploads'));
    app.use('/users', require('./users')(app));
};
