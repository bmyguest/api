module.exports = (app) => {
    const Media = app.models.Media;
    const Tag = app.models.Tag;
    const Group = app.models.Group;
    const User = app.models.User;
    const GroupMember = app.models.GroupMember;
    const GroupMedia = app.models.GroupMedia;
    const GroupTag = app.models.GroupTag;

    return (req, res, next) => {
        Group.findOne({
            where: { id: req.params.groupId },
            include: [ { model: Media, as: 'picture' }, { model: Media, as: 'medias', through: GroupMedia }, { model: Tag, through: GroupTag, as: 'tags' }, { model: User, as: 'owner' }, { model: User, through: GroupMember, as: 'members' } ]
        }).then((group) => {
            if (!group) return res.status(404).send('Group not found');
            if (group.deletedAt !== null) return res.status(404).send('Group deleted');
            req.group = group;
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};