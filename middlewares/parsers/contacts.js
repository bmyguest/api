module.exports = (app) => {
    const Evaluation = app.models.Evaluation;
    const Media = app.models.Media;
    const Tag = app.models.Tag;
    const Event = app.models.Event;
    const Group = app.models.Group;
    const User = app.models.User;
    const UserContact = app.models.UserContact;
    const UserMedia = app.models.UserMedia;
    const UserTag = app.models.UserTag;

    return (req, res, next) => {
        User.findOne({
            where: { id: req.params.contactId },
            include: [ { model: Evaluation }, { model: Group }, { model: Event }, { model: Media, as: 'medias', through: UserMedia }, { model: Tag, through: UserTag, as: 'tags' }, { model: User, through: UserContact, as: 'contacts' } ]
        }).then((user) => {
            if (!user) return res.status(404).send('User not found');
            if (user.deletedAt !== null) return res.status(404).send('User deleted');
            req.contact = user;
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};