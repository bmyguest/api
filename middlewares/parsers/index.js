module.exports = (app) => {
    return {
        contacts: require('./contacts')(app),
        evaluations: require('./evaluations')(app),
        events: require('./events')(app),
        groups: require('./groups')(app),
        medias: require('./medias')(app),
        users: require('./users')(app),
    }
};