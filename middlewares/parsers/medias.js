module.exports = (app) => {
    const Media = app.models.Media;

    return (req, res, next) => {
        Media.findOne({
            where: { id: req.params.mediaId}
        }).then((media) => {
            if (!media) return res.status(404).send('Media not found');
            req.media = media;
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};