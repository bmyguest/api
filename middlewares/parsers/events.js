module.exports = (app) => {
    const Evaluation = app.models.Evaluation;
    const Media = app.models.Media;
    const Tag = app.models.Tag;
    const Event = app.models.Event;
    const User = app.models.User;
    const EventParticipant = app.models.EventParticipant;
    const EventEvaluation = app.models.EventEvaluation;
    const EventMedia = app.models.EventMedia;
    const EventTag = app.models.EventTag;

    return (req, res, next) => {
        Event.findOne({
            where: { id: req.params.eventId },
            include: [ { model: Evaluation, through: EventEvaluation }, { model: Media, as: 'picture' }, { model: Media, as: 'medias', through: EventMedia }, { model: Tag, through: EventTag, as: 'tags' }, { model: User, as: 'owner' }, { model: User, through: EventParticipant, as: 'participants' } ]
        }).then((event) => {
            if (!event) return res.status(404).send('Event not found');
            if (event.deletedAt !== null) return res.status(404).send('Event deleted');
            req.event = event;
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};