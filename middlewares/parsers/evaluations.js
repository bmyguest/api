module.exports = (app) => {
    const Evaluation = app.models.Evaluation;
    const User = app.models.User;

    return (req, res, next) => {
        Evaluation.findOne({
            where: { id: req.params.evaluationId },
            include: [ { model: User }]
        }).then((evaluation) => {
            if (!evaluation) return res.status(404).send('Evaluation not found');
            req.evaluation = evaluation;
            next();
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};