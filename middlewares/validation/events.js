module.exports = (app) => {
    const Media = app.models.Media;

    return (req, res, next) => {
        if (!req.body || !req.body.name || !req.body.description || !req.body.start || !req.body.end || req.body.visibility === undefined || req.body.latitude === undefined || req.body.longitude === undefined || req.body.pictureId === undefined) {
            return res.status(400).send('Missing event fields (name, description, start, end, latitude, longitude, capacity?, recurrence?, visibility, pictureId)');
        }

        if (req.body.name.length < 2) {
            return res.status(400).send('Wrong name value. (min length: 2)');
        }

        if (req.body.description.length < 2) {
            return res.status(400).send('Wrong description value. (min length: 2)');
        }

        let start = Date.parse(req.body.start);
        if (isNaN(start)) {
            return res.status(400).send('Wrong start value.');
        }

        let end = Date.parse(req.body.end);
        if (isNaN(end)) {
            return res.status(400).send('Wrong end value.');
        }

        let latitude = Number(req.body.latitude);
        if (isNaN(latitude)) {
            return res.status(400).send('Wrong latitude value.');
        }

        let longitude = Number(req.body.longitude);
        if (isNaN(longitude)) {
            return res.status(400).send('Wrong longitude value.');
        }

        if (req.body.capacity) {
            let capacity = Number(req.body.capacity);
            if (isNaN(capacity) || capacity < 0) {
                return res.status(400).send('Wrong capacity value. (min: 0)');
            }
        }

        if (req.body.recurrence) {
            let recurrence = Number(req.body.recurrence);
            if (isNaN(recurrence) || recurrence < 0) {
                return res.status(400).send('Wrong recurrence value. (min: 0)');
            }
        }

        let visibility = Number(req.body.visibility);
        if (isNaN(visibility) || visibility < 0 || visibility > 2) {
            return res.status(400).send('Wrong visibility value. (0: private, 1: public, 2: premium)');
        }

        Media.findOne({ where: { id: req.body.pictureId } }).then((media) => {
            if (!media) return res.status(404).send('Picture not found');
            req.media = media;
            next();
        }).catch((err) => {
            return res.status(500).send(err.message);
        });
    }
};
