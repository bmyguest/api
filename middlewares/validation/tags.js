module.exports = (app) => {
    return (req, res, next) => {
        if (!req.body || !req.body.name) {
            return res.status(400).send('Missing tag fields (name)');
        }

        next();
    }
};
