module.exports = (app) => {
    return (req, res, next) => {
        if (!req.body || req.body.status === undefined) {
            return res.status(400).send('Missing status fields (status)');
        }

        let status = Number(req.body.status);
        if (isNaN(status) || status < -1 || status > 1) {
            return res.status(400).send('Wrong status value. (-1: rejected, 0: unanswered, 1: accepted)');
        }

        next();
    }
};
