module.exports = (app) => {
    return (req, res, next) => {
        if (!req.body || !req.body.file || req.body.categoryId === undefined) {
            return res.status(400).send('Missing media fields (file, categoryId)');
        }

        if (req.body.file.length < 2) {
            return res.status(400).send('Wrong file value. (min length: 2)');
        }

        let categoryId = Number(req.body.categoryId);
        if (isNaN(categoryId) || categoryId < 1 || categoryId > 5) {
            return res.status(400).send('Wrong categoryId value. (1: picture, 2: document, 3: video, 4: file, 5: audio)');
        }

        if (req.body.visibility) {
            let visibility = Number(req.body.visibility);
            if (isNaN(visibility) || visibility < 0 || visibility > 2) {
                return res.status(400).send('Wrong visibility value. (0: private, 1: public, 2: premium)');
            }
        }

        next();
    }
};
