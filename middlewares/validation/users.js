module.exports = (app) => {
    const Media = app.models.Media;

    return (req, res, next) => {
        if (!req.body || !req.body.id || !req.body.name || !req.body.gender || !req.body.picture || !req.body.birthday || !req.body.email) {
            return res.status(400).send('Missing group fields (id, name, email, birthday, gender, location?, picture)');
        }

        if (req.body.id.length < 2) {
            return res.status(400).send('Wrong id value. (min length: 2)');
        }

        if (req.body.name.length < 2) {
            return res.status(400).send('Wrong name value. (min length: 2)');
        }

        if (req.body.email.length < 2) {
            return res.status(400).send('Wrong email value. (min length: 2)');
        }

        if (req.body.birthday.length < 2) {
            return res.status(400).send('Wrong birthday value. (min length: 2)');
        }

        if (req.body.gender.length < 1) {
            return res.status(400).send('Wrong gender value. (min length: 1)');
        }

        if (req.body.location && req.body.location.length < 1) {
            return res.status(400).send('Wrong location value. (min length: 1)');
        }

        if (req.body.picture.length < 5) {
            return res.status(400).send('Wrong picture value. (min length: 5)');
        }

        next();
    }
};
