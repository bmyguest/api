module.exports = (app) => {
    const Media = app.models.Media;

    return (req, res, next) => {
        if (!req.body || !req.body.name || !req.body.description || req.body.open === undefined || req.body.visibility === undefined || req.body.pictureId === undefined) {
            return res.status(400).send('Missing group fields (name, description, open, visibility, pictureId)');
        }

        if (req.body.name.length < 2) {
            return res.status(400).send('Wrong name value. (min length: 2)');
        }

        if (req.body.description.length < 2) {
            return res.status(400).send('Wrong description value. (min length: 2)');
        }

        let open = Number(req.body.open);
        if (isNaN(open) || open < 0 || open > 2) {
            return res.status(400).send('Wrong open value. (0: private, 1: public, 2: premium)');
        }

        let visibility = Number(req.body.visibility);
        if (isNaN(visibility) || visibility < 0 || visibility > 2) {
            return res.status(400).send('Wrong visibility value. (0: private, 1: public, 2: premium)');
        }

        Media.findOne({ where: { id: req.body.pictureId } }).then((media) => {
            if (!media) return res.status(404).send('Picture not found');
            req.media = media;
            next();
        }).catch((err) => {
            return res.status(500).send(err.message);
        });
    }
};
