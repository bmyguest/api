module.exports = (app) => {
    return {
        tags: require('./tags')(app),
        evaluations: require('./evaluations')(app),
        events: require('./events')(app),
        groups: require('./groups')(app),
        medias: require('./medias')(app),
        status: require('./status')(app),
        users: require('./users')(app)
    }
};