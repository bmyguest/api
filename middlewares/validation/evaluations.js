module.exports = (app) => {
    return (req, res, next) => {
        if (!req.body || req.body.note === undefined) {
            return res.status(400).send('Missing evaluation fields (note, comment?)');
        }

        let note = Number(req.body.note);
        if (isNaN(note) || note < 0  || note > 10) {
            return res.status(400).send('Wrong note value. (min: 0, max: 10)');
        }

        next();
    }
};
