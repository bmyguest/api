module.exports = (app) => {
    return {
        create: require('./create')(app),
        update: require('./update')(app),
        findAll: require('./findAll')(app),
        findOne: require('./findOne')(app),
        remove: require('./remove')(app)
    }
};
