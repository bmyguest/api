module.exports = (app) => {
    return function create(req, res, next) {
        req.user.createEvaluation(req.body).then((evaluation) => {
            evaluation.setUser(req.loggedUser);
            res.send(evaluation);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};