module.exports = (app) => {
    const GroupMember = app.models.GroupMember;
    const GroupTag = app.models.GroupTag;
    const Media = app.models.Media;
    const Tag = app.models.Tag;
    const User = app.models.User;
    
    return function findAll(req, res, next) {
        let where = {};

        if (req.query.search) {
            where.$or = [
                { name: { $like: `%${req.query.search}%` } },
                { description: { $like: `%${req.query.search}%` } }
            ]
        }

        req.user.getMemberships({
            where: where,
            through: { where: { status: 1 } },
            include: [ { model: User, as: 'members', through: GroupMember }, { model: Tag, as: 'tags', through: GroupTag }, { model: Media, as: 'picture' }, { model: User, as: 'owner' } ]
        }).then((membership) => {
            res.send(membership);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
