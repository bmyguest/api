module.exports = (app) => {
    return {
        create: require('./create')(app),
        findAll: require('./findAll')(app),
        findOne: require('./findOne')(app),
        remove: require('./remove')(app),
        evaluations: require('./evaluations')(app),
        medias: require('./medias')(app),
        contacts: require('./contacts')(app),
        events: require('./events')(app),
        participations: require('./participations')(app),
        memberships: require('./memberships')(app),
        groups: require('./groups')(app),
        tags: require('./tags')(app)
    }
};
