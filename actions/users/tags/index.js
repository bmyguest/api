module.exports = (app) => {
    return {
        create: require('./create')(app),
        findAll: require('./findAll')(app),
        remove: require('./remove')(app)
    }
};
