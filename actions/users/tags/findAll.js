module.exports = (app) => {
    return function findAll(req, res, next) {
        req.user.getTags().then((tags) => {
            res.send(tags);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
