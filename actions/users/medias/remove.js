module.exports = (app) => {
    return function remove(req, res, next) {
        req.user.removeMedia(req.params.mediaId).then((count) => {
            if (!count) return res.status(404).send('Media not found');
            req.media.destroy();
            res.send({ count });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};