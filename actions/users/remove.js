module.exports = (app) => {
    return function remove(req, res, next) {
        req.user.destroy().then((user) => {
            res.send(user);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};