module.exports = (app) => {
    const EventParticipant = app.models.EventParticipant;
    const EventTag = app.models.EventTag;
    const Media = app.models.Media;
    const Tag = app.models.Tag;
    const User = app.models.User;

    return function findAll(req, res, next) {
        let where = {};

        if (req.query.search) {
            where.$or = [
                { name: { $like: `%${req.query.search}%` } },
                { description: { $like: `%${req.query.search}%` } }
            ]
        }

        req.user.getParticipations({
            where: where,
            include: [ { model: User, as: 'participants', through: EventParticipant }, { model: Tag, as: 'tags', through: EventTag }, { model: Media, as: 'picture' }, { model: User, as: 'owner' } ]
        }).then((participations) => {
            res.send(participations);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
