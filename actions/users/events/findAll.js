module.exports = (app) => {
    const EventEvaluation = app.models.EventEvaluation;
    const EventMedia = app.models.EventMedia;
    const EventTag = app.models.EventTag;
    const EventParticipant = app.models.EventParticipant;
    const Evaluation = app.models.Evaluation;
    const Media = app.models.Media;
    const Tag = app.models.Tag;
    const User = app.models.User;

    return function findAll(req, res, next) {
        let where = {};

        if (req.query.search) {
            where.$or = [
                { name: { $like: `%${req.query.search}%` } },
                { description: { $like: `%${req.query.search}%` } }
            ]
        }

        req.user.getEvents({
            where: where,
            include: [ { model: User, as: 'owner' }, { model: User, as: 'participants', through: EventParticipant }, { model: Media, as: 'medias', through: EventMedia }, { model: Evaluation, as: 'evaluations', through: EventEvaluation }, { model: Tag, as: 'tags', through: EventTag }, { model: Media, as: 'picture' } ]
        }).then((events) => {
            res.send(events);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
