module.exports = (app) => {
    return function create(req, res, next) {
        req.loggedUser.addContact(req.user.id).then((contact) => {
            req.user.addContact(req.loggedUser);
            res.send({ count: 1 });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};