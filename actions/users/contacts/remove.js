module.exports = (app) => {
    return function remove(req, res, next) {
        req.user.removeContacts(req.contact.id).then((count) => {
            req.contact.removeContact(req.user.id);
            res.send({ count: count });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};