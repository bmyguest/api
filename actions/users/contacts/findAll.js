module.exports = (app) => {
    const Tag = app.models.Tag;
    const UserTag = app.models.UserTag;
    const STATUS_ACCEPTED = 1;

    return function findAll(req, res, next) {
        let where = {};

        if (req.query.search) {
            where.$or = [
                { name: { $like: `%${req.query.search}%` } },
                { email: { $like: `%${req.query.search}%` } },
                { location: { $like: `%${req.query.search}%` } }
            ]
        }

        req.user.getContacts({
            where: where,
            through: { where: { status: STATUS_ACCEPTED } },
            include: [ { model: Tag, as: 'tags', through: UserTag } ]
        }).then((contacts) => {
            res.send(contacts);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
