module.exports = (app) => {
    return function update(req, res, next) {
        req.user.getContacts({ where: { id: req.contact.id } }).then((contacts) => {
            if (!contacts.length) return res.status(404).send('Contact not found');
            contacts[0].userscontact.updateAttributes(req.body);

            req.contact.getContacts({ where: { id: req.user.id } }).then((contacts) => {
                if (!contacts.length) return res.status(404).send('Contact not found');
                contacts[0].userscontact.updateAttributes(req.body);
                res.send(contacts[0]);
            }).catch((err) => {
                res.status(500).send(err.message);
            });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};