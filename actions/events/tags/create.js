module.exports = (app) => {
    return function create(req, res, next) {
        req.event.createTag(req.body).then((tag) => {
            res.send(tag);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};