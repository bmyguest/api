module.exports = (app) => {
    const Evaluation = app.models.Evaluation;
    const Media = app.models.Media;
    const EventMedia = app.models.EventMedia;
    const EventTag = app.models.EventTag;
    const Tag = app.models.Tag;
    const Event = app.models.Event;
    const User = app.models.User;
    const EventParticipant = app.models.EventParticipant;

    return (req, res, next) => {
        let where = {};

        if (req.query.search) {
            where.$or = [
                { name: { $like: `%${req.query.search}%` } },
                { description: { $like: `%${req.query.search}%` } }
            ]
        }

        Event.findAll({
            where: where,
            include: [ { model: Evaluation }, { model: Media, as: 'picture' }, { model: Media, as: 'medias', through: EventMedia }, { model: Tag, through: EventTag, as: 'tags' }, { model: User, as: 'owner' }, { model: User, through: EventParticipant, as: 'participants' } ]
        }).then((events) => {
            if (!events) return res.sendStatus(204);
            res.send(events);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
