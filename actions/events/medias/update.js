module.exports = (app) => {
    return function update(req, res, next) {
        req.event.getMedias({ where: { id: req.params.mediaId } }).then((medias) => {
            if (!medias.length) return res.status(404).send('Media not found');
            medias.forEach((media) => { media.updateAttributes(req.body) });
            res.send(medias);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};