module.exports = (app) => {
    return function update(req, res, next) {
        req.event.getParticipants({ where: { id: req.params.participantId } }).then((participants) => {
            if (!participants.length) return res.status(404).send('Participant not found');
            participants.forEach((participant) => { participant.eventsparticipant.updateAttributes(req.body) });
            res.send(participants);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};