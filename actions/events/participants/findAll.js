module.exports = (app) => {
    return function findAll(req, res, next) {
        req.event.getParticipants().then((participants) => {
            res.send(participants);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
