module.exports = (app) => {
    return function remove(req, res, next) {
        req.event.removeParticipant(req.params.participantId).then((count) => {
            if (!count) return res.status(404).send('Participant not found');
            res.send({ count });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};