module.exports = (app) => {
    return function create(req, res, next) {
        req.event.addParticipant(req.loggedUser.id).then((participant) => {
            if (!participant.length) return res.status(404).send('Already participating');
            res.send({ count: 1 });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};