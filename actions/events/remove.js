module.exports = (app) => {
    return function remove(req, res, next) {
        req.loggedUser.removeEvent(req.params.eventId).then((events) => {
            if (!events) return res.status(404).send('Event not found');
            req.event.destroy();
            res.send({ count: 1 });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};