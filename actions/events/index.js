module.exports = (app) => {
    return {
        findAll: require('./findAll')(app),
        findOne: require('./findOne')(app),
        create: require('./create')(app),
        update: require('./update')(app),
        remove: require('./remove')(app),
        evaluations: require('./evaluations')(app),
        medias: require('./medias')(app),
        participants: require('./participants')(app),
        tags: require('./tags')(app)
    }
};
