module.exports = (app) => {
    return function create(req, res, next) {
        req.loggedUser.createEvent(req.body).then((event) => {
            event.addMedia(req.media.id).then((media) => {
                event.addParticipant(req.loggedUser.id);
                res.send(event);
            });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
