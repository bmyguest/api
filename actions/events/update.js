module.exports = (app) => {
    return function update(req, res, next) {
        req.loggedUser.getEvents({ where: { id: req.params.eventId } }).then((events) => {
            if (!events.length) return res.status(404).send('Event not found');
            events.forEach((event) => { event.updateAttributes(req.body) });
            res.send(events);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};