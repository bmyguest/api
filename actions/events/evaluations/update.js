module.exports = (app) => {
    return function update(req, res, next) {
        req.event.getEvaluations({ where: { id: req.params.evaluationId } }).then((evaluations) => {
            if (!evaluations.length) return res.status(404).send('Evaluation not found');
            evaluations.forEach((evaluation) => { evaluation.updateAttributes(req.body) });
            res.send(evaluations);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};