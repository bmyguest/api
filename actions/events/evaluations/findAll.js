module.exports = (app) => {
    const User = app.models.User;

    return function findAll(req, res, next) {
        req.event.getEvaluations({
            include: [ { model: User } ]
        }).then((evaluations) => {
            res.send(evaluations);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
