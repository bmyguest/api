module.exports = (app) => {
    return function create(req, res, next) {
        req.event.createEvaluation(req.body).then((evaluation) => {
            req.loggedUser.addEvaluation(evaluation);
            res.send(evaluation);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};