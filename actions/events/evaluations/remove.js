module.exports = (app) => {
    return function remove(req, res, next) {
        req.event.removeEvaluation(req.evaluation.id).then((count) => {
            if (!count) return res.status(404).send('Evaluation not found');
            req.evaluation.destroy();
            res.send({ count });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};