module.exports = (app) => {
    const Media = app.models.Media;
    const MediaCategory = app.models.MediaCategory;

    return (req, res, next) => {
        Media.findAll({
            include: [ { model: MediaCategory, as: 'category' } ]
        }).then((medias) => {
            if (!medias) return res.sendStatus(204);
            res.send(medias);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
