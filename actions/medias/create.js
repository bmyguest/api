module.exports = (app) => {
    const Media = app.models.Media;
    const Visibility = {
        PRIVATE: 0,
        PUBLIC: 1
    };
    const MediaCategory = {
        IMAGE: 1
    };

    return function create(req, res, next) {
        let media = {
            file: app.settings.url + app.settings.uploadsDir + req.savedfilename,
            filename: req.file.filename,
            visibility: Visibility.PUBLIC,
            categoryId: MediaCategory.IMAGE
        };

        Media.build(media).save().then((media) => {
            if (!media) return res.status(500).send("Sh*t happened");
            res.send(media);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};