module.exports = (app) => {
    return {
        findAll: require('./findAll')(app),
        findOne: require('./findOne')(app),
        remove: require('./remove')(app),
        update: require('./update')(app),
        create: require('./create')(app),
        medias: require('./medias')(app),
        members: require('./members')(app),
        tags: require('./tags')(app)
    }
};
