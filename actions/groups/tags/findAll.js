module.exports = (app) => {
    return function findAll(req, res, next) {
        req.group.getTags().then((tags) => {
            res.send(tags);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
