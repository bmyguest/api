module.exports = (app) => {
    return function remove(req, res, next) {
        req.group.removeTag(req.params.tagId).then((count) => {
            res.send({ count: count });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};