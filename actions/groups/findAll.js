module.exports = (app) => {
    const Media = app.models.Media;
    const Tag = app.models.Tag;
    const Group = app.models.Group;
    const User = app.models.User;
    const GroupMedia = app.models.GroupMedia;
    const GroupMember = app.models.GroupMember;
    const GroupTag = app.models.GroupTag;

    return (req, res, next) => {
        Group.findAll({
            include: [ { model: Media, as: 'picture' }, { model: Media, as: 'medias', through: GroupMedia }, { model: Tag, through: GroupTag, as: 'tags' }, { model: User, as: 'owner' }, { model: User, through: GroupMember, as: 'members' } ]
        }).then((groups) => {
            if (!groups) return res.sendStatus(204);
            res.send(groups);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
