module.exports = (app) => {
    return function findAll(req, res, next) {
        req.group.getMedias().then((medias) => {
            res.send(medias);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
