module.exports = (app) => {
    return function create(req, res, next) {
        req.group.createMedia(req.body).then((media) => {
            res.send(media);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};