module.exports = (app) => {
    return function create(req, res, next) {
        req.loggedUser.createGroup(req.body).then((group) => {
            group.addMedia(req.media.id).then((media) => {
                group.addMember(req.loggedUser.id);
                res.send(group);
            });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};