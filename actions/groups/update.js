module.exports = (app) => {
    return function update(req, res, next) {
        req.loggedUser.getGroups({ where: { id: req.params.groupId } }).then((groups) => {
            if (!groups.length) return res.status(404).send('Group not found');
            groups.forEach((group) => { group.updateAttributes(req.body) });
            res.send(groups);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};