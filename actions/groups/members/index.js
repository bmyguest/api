module.exports = (app) => {
    return {
        create: require('./create')(app),
        findAll: require('./findAll')(app),
        update: require('./update')(app),
        remove: require('./remove')(app)
    }
};
