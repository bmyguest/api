module.exports = (app) => {
    return function create(req, res, next) {
        req.group.addMember(req.loggedUser.id).then((member) => {
            if (!member.length) return res.status(404).send('Already member');
            res.send({ count: 1 });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};