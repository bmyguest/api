module.exports = (app) => {
    return function update(req, res, next) {
        req.group.getMembers({ where: { id: req.params.memberId } }).then((members) => {
            if (!members.length) return res.status(404).send('Member not found');
            members.forEach((member) => { member.groupsmember.updateAttributes(req.body); });
            res.send(members);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};