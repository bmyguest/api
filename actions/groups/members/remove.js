module.exports = (app) => {
    return function remove(req, res, next) {
        req.group.removeMember(req.loggedUser.id).then((count) => {
            if (!count) return res.status(404).send('Member not found');
            res.send({ count: count });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};