module.exports = (app) => {
    const Media = app.models.Media;

    return function findAll(req, res, next) {
        req.group.getMembers({
            through: { where: { status: 1 } }
        }).then((members) => {
            res.send(members);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};
