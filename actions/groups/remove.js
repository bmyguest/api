module.exports = (app) => {
    return function remove(req, res, next) {
        req.loggedUser.removeGroup(req.params.groupId).then((groups) => {
            if (!groups) return res.status(404).send('Group not found');
            req.group.destroy();
            res.send({ count: 1 });
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};