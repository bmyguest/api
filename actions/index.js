module.exports = (app) => {
    console.log('Loading actions...');

    app.actions = {
        events: require('./events')(app),
        groups: require('./groups')(app),
        medias: require('./medias')(app),
        users: require('./users')(app)
    };
};