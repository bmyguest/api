module.exports = (app) => {
    console.log('Loading models...');

    app.models = {
        Evaluation: require('./mysql/evaluation')(app),
        Event: require('./mysql/event')(app),
        EventEvaluation: require('./mysql/eventsevaluation')(app),
        EventMedia: require('./mysql/eventsmedia')(app),
        EventParticipant: require('./mysql/eventsparticipant')(app),
        EventTag: require('./mysql/eventstag')(app),
        Group: require('./mysql/group')(app),
        GroupMedia: require('./mysql/groupsmedia')(app),
        GroupTag: require('./mysql/groupstag')(app),
        GroupMember: require('./mysql/groupsmember')(app),
        Media: require('./mysql/media')(app),
        MediaTag: require('./mysql/mediastag')(app),
        MediaCategory: require('./mysql/mediascategory')(app),
        Tag: require('./mysql/tag')(app),
        User: require('./mysql/user')(app),
        UserContact: require('./mysql/userscontact')(app),
        UserEvaluation: require('./mysql/usersevaluation')(app),
        UserMedia: require('./mysql/usersmedia')(app),
        UserTag: require('./mysql/userstag')(app)
    };

    app.models.Evaluation.belongsTo(app.models.User);

    app.models.Event.belongsToMany(app.models.Tag, { through: app.models.EventTag, as: 'tags' });
    app.models.Event.belongsTo(app.models.Media, { as: 'picture', foreignKey: 'pictureId' });
    app.models.Event.belongsTo(app.models.User, { as: 'owner', foreignKey: 'ownerId' });
    app.models.Event.belongsToMany(app.models.Evaluation, { through: app.models.EventEvaluation });
    app.models.Event.belongsToMany(app.models.Media, { through: app.models.EventMedia, as: 'medias', otherKey: 'mediaId' });
    app.models.Event.belongsToMany(app.models.User, { through: app.models.EventParticipant, as: 'participants' });

    app.models.Group.belongsToMany(app.models.Tag, { through: app.models.GroupTag, as: 'tags' });
    app.models.Group.belongsTo(app.models.Media, { as: 'picture', foreignKey: 'pictureId' });
    app.models.Group.belongsTo(app.models.User, { as: 'owner', foreignKey: 'ownerId' });
    app.models.Group.belongsToMany(app.models.Media, { through: app.models.GroupMedia, as: 'medias', otherKey: 'mediaId' });
    app.models.Group.belongsToMany(app.models.User, { through: app.models.GroupMember, as: 'members' });

    app.models.Media.belongsToMany(app.models.Tag, { through: app.models.MediaTag, as: 'tags' });
    app.models.Media.belongsTo(app.models.MediaCategory, { as: 'category', foreignKey: 'categoryId' });

    app.models.User.belongsToMany(app.models.Tag, { through: app.models.UserTag, as: 'tags' });
    app.models.User.belongsToMany(app.models.Evaluation, { through: app.models.UserEvaluation });
    app.models.User.belongsToMany(app.models.Event, { through: app.models.EventParticipant, as: 'participations' });
    app.models.User.belongsToMany(app.models.Group, { through: app.models.GroupMember, as: 'memberships' });
    app.models.User.hasMany(app.models.Evaluation);
    app.models.User.hasMany(app.models.Event, { foreignKey: 'ownerId' });
    app.models.User.hasMany(app.models.Group, { foreignKey: 'ownerId' });
    app.models.User.belongsToMany(app.models.Media, { through: app.models.UserMedia, as: 'medias', otherKey: 'mediaId' });
    app.models.User.belongsToMany(app.models.User, { through: app.models.UserContact, as: 'contacts' });
};
