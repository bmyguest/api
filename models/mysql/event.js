const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('event', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        start: {
            type: Sequelize.DATE,
            allowNull: false,
            defaultValue: Sequelize.NOW
        },
        end: {
            type: Sequelize.DATE,
            allowNull: false,
        },
        latitude: {
            type: Sequelize.DECIMAL(10,7),
            allowNull: false
        },
        longitude: {
            type: Sequelize.DECIMAL(10,7),
            allowNull: false
        },
        capacity: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        recurrence: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        visibility: {
            type: Sequelize.INTEGER(1),
            defaultValue: 0,
            allowNull: false
        }
    }, { paranoid: true });
};
