const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('group', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        open: {
            type: Sequelize.INTEGER(1),
            defaultValue: 0,
            allowNull: false
        },
        visibility: {
            type: Sequelize.INTEGER(1),
            defaultValue: 0,
            allowNull: false
        }
    }, { paranoid: true });
};
