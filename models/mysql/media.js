const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('media', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        file: {
            type: Sequelize.STRING,
            allowNull: false
        },
        visibility: {
            type: Sequelize.INTEGER(1),
            defaultValue: 1,
            allowNull: false
        }
    }, { paranoid: true, tableName: 'medias' });
};
