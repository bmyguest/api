const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('user', {
        id: {
            type: Sequelize.STRING,
            primaryKey: true,
            allowNull: false
        },
        token: {
            type: Sequelize.STRING,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false
        },
        birthday: {
            type: Sequelize.STRING,
            allowNull: false
        },
        gender: {
            type: Sequelize.STRING,
            allowNull: false
        },
        location: {
            type: Sequelize.STRING,
            allowNull: true
        },
        picture: {
            type: Sequelize.STRING,
            allowNull: false
        },
        premium: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        }
    }, {
        paranoid: true,
        hooks: {
            beforeDestroy: (user) => {
                user.events.forEach((event) => { event.destroy() });
                user.groups.forEach((group) => { group.destroy() });
            }
        }
    });
};
