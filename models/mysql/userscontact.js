const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('userscontact', {
        status: {
            type: Sequelize.INTEGER(1),
            defaultValue: 1,
            allowNull: false
        }
    });
};
