const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('mediascategory', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });
};
