module.exports = (app) => {
    const MediaCategory = app.models.MediaCategory;
    const Tag = app.models.Tag;

    console.log('Loading fixtures...');

    app.sequelize.sync().then(() => {
        Tag.findOrCreate({ where: { name: "Indoors" } });
        Tag.findOrCreate({ where: { name: "Outdoors" } });
        Tag.findOrCreate({ where: { name: "House" } });
        Tag.findOrCreate({ where: { name: "Clubbing" } });
        Tag.findOrCreate({ where: { name: "Afterwork" } });
        Tag.findOrCreate({ where: { name: "Conference" } });
        Tag.findOrCreate({ where: { name: "Bar" } });
        Tag.findOrCreate({ where: { name: "Concert" } });
        Tag.findOrCreate({ where: { name: "Apartment" } });
        Tag.findOrCreate({ where: { name: "Protest" } });
        Tag.findOrCreate({ where: { name: "Fun" } });
        Tag.findOrCreate({ where: { name: "Park" } });
        Tag.findOrCreate({ where: { name: "Beach" } });
        Tag.findOrCreate({ where: { name: "Campfire" } });
        Tag.findOrCreate({ where: { name: "Booze" } });
        Tag.findOrCreate({ where: { name: "Dating" } });

        MediaCategory.findOrCreate({ where: { name: "Picture" } });
        MediaCategory.findOrCreate({ where: { name: "Video" } });
        MediaCategory.findOrCreate({ where: { name: "Audio" } });
        MediaCategory.findOrCreate({ where: { name: "Document" } });
        MediaCategory.findOrCreate({ where: { name: "File" } });
    });
};
